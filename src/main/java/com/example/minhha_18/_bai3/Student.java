package com.example.minhha_18._bai3;

import lombok.Data;

@Data

public class Student extends Person{
    private String tenLop;
    private float diem;

    public Student() {
    }

    public Student(String tenLop) {
        this.tenLop = tenLop;
    }

    public Student(String tenLop, float diem) {
        this.tenLop = tenLop;
        this.diem = diem;
    }

    public Student(String ten, int tuoi, String gioiTinh) {
        super(ten, tuoi, gioiTinh);
        //gọi đến, tham chiếu đến chính các thuộc tính của class Person (là lớp cha gần nhất của Student)
        //cách dùng cũng giống this.tenLop sẽ gọi đến thuộc tính tenLop ở class Student thì super(ten) sẽ gọi đến thuộc tính ten ở class Person
    }

    public Student(String ten, int tuoi, String gioiTinh, String tenLop, float diem) {
        super(ten, tuoi, gioiTinh);
        this.tenLop = tenLop;
        this.diem = diem;
    }

    @Override
    public String printHello() {
        return "Hello, I am a Student";
    }

    public String printHello(int tuoi) {
        return "Hello, I am a student, i am " + tuoi + "years old";
    }

    public String printHello(String ten, int tuoi) {
        return "Hello, I am a student, my name " + ten + ", i am " + tuoi + "years old";
    }

    public String printHello(String ten, int tuoi, String gioiTinh, String tenLop, float diem) {
        return "Hello + 5: " + ten + tuoi + gioiTinh + tenLop + diem;
    }

    //Ko thể Override lại hàm private hello() trong class Person từ 2 class Student và Teacher (câu 8) dẫn đ
    public String hello() {
        return "Hoc sinh ngoan";
    }

    public String hello(String ten) {
        return "Hoc sinh ngoannnnnn";
    }
}
