package com.example.minhha_18._bai3;

import lombok.*;
import org.springframework.stereotype.Component;

@Data
@Component

public class PersonData {
    private Student student = new Student();
    private Student student1 = new Student("Lop 1");
    private Student student2 = new Student("Lop 2", 9.5f);
    private Student student3 = new Student("Minh", 23, "nam", "Lop 1", 9.7f);
    private Teacher teacher = new Teacher();
    private Teacher teacher1 = new Teacher("Doc than");
    private Teacher teacher2 = new Teacher(10, "Doc than");
    private Teacher teacher3 = new Teacher("Minh", 23, "Nam", 2, "Doc than");
}