package com.example.minhha_18._bai3;

import com.example.minhha_18._bai3.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/bai3")

public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/getPerson")
    public ResponseEntity<ResponseApi> getPerson() {
        return new ResponseEntity<>(personService.getPerson(), HttpStatus.OK);
    }

    @GetMapping("/getStudent")
    public ResponseEntity<ResponseApi> getStudent() {
        return new ResponseEntity<>(personService.getStudent(), HttpStatus.OK);
    }

    @GetMapping("/getTeacher")
    public ResponseEntity<ResponseApi> getTeacher() {
        return new ResponseEntity<>(personService.getTeacher(), HttpStatus.OK);
    }

    @GetMapping("/helloStudent")
    public ResponseEntity<ResponseApi> getHelloStudent() {
        return new ResponseEntity<>(personService.helloStudent(), HttpStatus.OK);
    }

    @GetMapping("/helloTeacher")
    public ResponseEntity<ResponseApi> getHelloTeacher() {
        return new ResponseEntity<>(personService.helloTeacher(), HttpStatus.OK);
    }
}
