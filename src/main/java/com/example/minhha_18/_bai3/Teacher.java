package com.example.minhha_18._bai3;

import lombok.Data;

@Data

public class Teacher extends Person {
    private int namNgheNghiep;
    private String tinhTrangHonNhan;

    public Teacher() {
    }

    public Teacher(String tinhTrangHonNhan) {
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }

    public Teacher(int namNgheNghiep, String tinhTrangHonNhan) {
        this.namNgheNghiep = namNgheNghiep;
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }

    public Teacher(String ten, int tuoi, String gioiTinh) {
        super(ten, tuoi, gioiTinh);
    }

    public Teacher(String ten, int tuoi, String gioiTinh, int namNgheNghiep, String tinhTrangHonNhan) {
        super(ten, tuoi, gioiTinh);
        this.namNgheNghiep = namNgheNghiep;
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }

    @Override
    public String printHello() {
        return "Hello, I am a Teacher";
    }

    public String printHello(int namNgheNghiep) {
        return "Hello, I am a Teacher, I am " + namNgheNghiep;
    }

    public String printHello(int namNgheNghiep, String tinhTrangHonNhan) {
        return "Hello, I am a Teacher, I am " + namNgheNghiep + " I am " + tinhTrangHonNhan;
    }


    public String hello() {
        return "Co giao tot";
    }
}
