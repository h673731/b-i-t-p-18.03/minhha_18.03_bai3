package com.example.minhha_18._bai3.service;

import com.example.minhha_18._bai3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component

public class PersonImpl implements PersonService {

    @Autowired
    private PersonData personData;

    @Override
    public ResponseApi getPerson() {
        List<Person> personList = new ArrayList<>();
        personList.add(personData.getStudent());
        personList.add(personData.getStudent());
        personList.add(personData.getStudent());
        personList.add(personData.getStudent());
        personList.add(personData.getTeacher());
        personList.add(personData.getTeacher1());
        personList.add(personData.getTeacher2());
        personList.add(personData.getTeacher3());
        return new ResponseApi(personList);
    }

    @Override
    public ResponseApi getStudent() {
        List<Student> studentList = new ArrayList<>();
        studentList.add(personData.getStudent());
        studentList.add(personData.getStudent());
        studentList.add(personData.getStudent());
        studentList.add(personData.getStudent());
        return new ResponseApi(studentList);
    }

    @Override
    public ResponseApi getTeacher() {
        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(personData.getTeacher());
        teacherList.add(personData.getTeacher1());
        teacherList.add(personData.getTeacher2());
        teacherList.add(personData.getTeacher3());
        return new ResponseApi(teacherList);
    }

    @Override
    public ResponseApi helloStudent() {
        List<String> helloStudentList = new ArrayList<>();
        helloStudentList.add(personData.getStudent().hello());
        helloStudentList.add(personData.getStudent1().printHello("Minh",23));
        helloStudentList.add(personData.getStudent().printHello(21));
        helloStudentList.add(personData.getStudent1().printHello(22));
        helloStudentList.add(personData.getStudent2().printHello(23));
        return new ResponseApi(helloStudentList);
    }

    @Override
    public ResponseApi helloTeacher() {
        List<String> helloTeacherList = new ArrayList<>();
        helloTeacherList.add(personData.getTeacher().hello());
        helloTeacherList.add(personData.getTeacher3().printHello(20, "Co chong"));
        return new ResponseApi(helloTeacherList);
    }
}
