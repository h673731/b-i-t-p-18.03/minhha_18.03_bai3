package com.example.minhha_18._bai3.service;

import com.example.minhha_18._bai3.ResponseApi;
import org.springframework.stereotype.Service;

@Service

public interface PersonService {
    public ResponseApi getPerson();
    public ResponseApi getStudent();
    public ResponseApi getTeacher();
    public ResponseApi helloStudent();
    public ResponseApi helloTeacher();
}
